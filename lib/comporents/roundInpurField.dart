import 'package:flutter/material.dart';
import 'package:loginui/comporents/textFeild_container.dart';

import '../constants.dart';

class RoundInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChange;

  RoundInputField({
    this.hintText,
    this.icon = Icons.person,
    this.onChange,
  });

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child_tfContainer: TextField(
        onChanged: onChange,
        decoration: InputDecoration(
          hintText: hintText,
          border: InputBorder.none,
          icon: Icon(
            icon,
            color: kPrimaryColor,
          ),
        ),
      ),
    );
  }
}
