import 'package:flutter/material.dart';

import '../constants.dart';

class AlradyHaveAnAccountCheck extends StatelessWidget {
  final bool login;
  final Function press;

  AlradyHaveAnAccountCheck({
    this.login = true,
    this.press,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          login ? 'Don\'t have an Account ? ' : 'Alrady have an ASccount ? ',
          style: TextStyle(
            color: kPrimaryColor,
          ),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            login ? 'Sign UP' : 'Sigh IN',
            style: TextStyle(
              color: kPrimaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }
}
