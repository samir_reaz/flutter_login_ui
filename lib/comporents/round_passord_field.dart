import 'package:flutter/material.dart';
import 'package:loginui/comporents/textFeild_container.dart';

import '../constants.dart';

class RoundPasswordField extends StatelessWidget {
  final ValueChanged<String> onChnge;

  RoundPasswordField({this.onChnge});

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child_tfContainer: TextField(
        onChanged: onChnge,
        obscureText: true,
        decoration: InputDecoration(
            hintText: 'Password',
            icon: Icon(
              Icons.lock,
              color: kPrimaryColor,
            ),
            suffixIcon: Icon(
              Icons.visibility,
              color: kPrimaryColor,
            ),
            border: InputBorder.none),
      ),
    );
  }
}
