import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:loginui/Screen/Login/login_screen.dart';
import 'package:loginui/Screen/Welcome/components/background.dart';
import 'package:loginui/Screen/Welcome/components/round_button.dart';
import 'package:loginui/constants.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child_my: SingleChildScrollView(
        //scrollDirection: Axis.vertical,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'WELCOME TO EDU',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            SvgPicture.asset(
              'icons/chat.svg',
              height: size.height * 0.5,
            ),
            SizedBox(height: size.height * 0.05),
            RoundButton(
              text: 'LOGIN',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            RoundButton(
              text: 'LOGIN',
              textColor: Colors.black,
              color: kPrimaryLightColor,
              press: () {},
            ),
          ],
        ),
      ),
    );
  }
}
