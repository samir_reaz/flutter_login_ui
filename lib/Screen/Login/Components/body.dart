import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:loginui/Screen/Login/Components/background.dart';
import 'package:loginui/comporents/roundInpurField.dart';
import 'package:loginui/comporents/round_button.dart';
import 'package:loginui/comporents/round_passord_field.dart';
import 'package:loginui/Screen/SingUp/sighup_screen.dart';
import 'package:loginui/comporents/alrady_have_an_account_check.dart';

class Body extends StatefulWidget {
  final Widget child_login;

  const Body({
    Key key,
    @required this.child_login,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    String _email, _password;

    return Background(
      child_login: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "LOGIN",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: size.height * 0.03),
          SvgPicture.asset(
            'icons/login.svg',
            width: size.height * 0.45,
          ),
          SizedBox(height: size.height * 0.03),
          RoundInputField(
            hintText: 'Example@info.xyz',
            onChange: (value) {
              _email = value;
            },
          ),
          RoundPasswordField(
            onChnge: (value) {
              _password = value;
            },
          ),
          RoundButton(
            text: 'LOGIN',
            paress: () {},
          ),
          SizedBox(height: size.height * 0.03),
          AlradyHaveAnAccountCheck(
            login: false,
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return SignUpScreen();
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }
}
