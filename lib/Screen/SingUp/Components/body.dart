import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:loginui/Screen/Login/login_screen.dart';
import 'package:loginui/Screen/SingUp/Components/socal_icon.dart';
import 'package:loginui/Screen/Welcome/components/round_button.dart';
import 'package:loginui/comporents/alrady_have_an_account_check.dart';
import 'package:loginui/comporents/roundInpurField.dart';
import 'package:loginui/comporents/round_passord_field.dart';

import '../../../constants.dart';
import 'backgound.dart';
import 'or_divider.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child_signup: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('SIGNUP'),
          SizedBox(height: size.height * .01),
          SvgPicture.asset(
            'icons/signup.svg',
            height: size.height * .4,
          ),
          RoundInputField(hintText: 'Your E-Mail'),
          RoundPasswordField(),
          RoundButton(
            text: 'SIGNUP',
            color: kPrimaryColor,
            textColor: Colors.white,
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return LoginScreen();
                  },
                ),
              );
            },
          ),
          SizedBox(
            height: size.height * .02,
          ),
          AlradyHaveAnAccountCheck(
            login: false,
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return LoginScreen();
                  },
                ),
              );
            },
          ),
          OrDivider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SocalIcon(
                iconSrc: 'icons/facebook.svg',
                press: () {},
              ),
              SocalIcon(
                iconSrc: 'icons/twitter.svg',
              ),
              SocalIcon(
                iconSrc: 'icons/google-plus.svg',
              ),
            ],
          ),
        ],
      ),
    );
  }
}
